FROM alpine:latest
RUN apk add --no-cache curl
RUN adduser -D user
COPY test /
ENTRYPOINT ["/bin/sh"]
USER user
